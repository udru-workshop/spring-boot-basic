CREATE TABLE product (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    description TEXT,
    price DECIMAL(10, 2),
    quantity INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


INSERT INTO product (name, description, price, quantity) VALUES
('Product 1', 'Description of product 1', 19.99, 10),
('Product 2', 'Description of product 2', 29.99, 20),
('Product 3', 'Description of product 3', 39.99, 30),
('Product 4', 'Description of product 4', 49.99, 40);

