package com.example.demo.service;

import com.example.demo.constant.ReturnCode;
import com.example.demo.entity.Product;
import com.example.demo.model.ProductData;
import com.example.demo.model.request.CreateProductRequest;
import com.example.demo.model.request.UpdateProductRequest;
import com.example.demo.model.response.BaseResponse;
import com.example.demo.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    private final Logger log = LoggerFactory.getLogger(ProductService.class);

    public BaseResponse createProduct(CreateProductRequest request) {
        log.info("create product.");
        BaseResponse resp = new BaseResponse(ReturnCode.TXN001);

        log.info("copy field from request to entity model");
        Product product = new Product();
        product.setName(request.getName());
        product.setDescription(request.getDescription());
        product.setPrice(request.getPrice());
        product.setQuantity(request.getQuantity());

        productRepository.save(product);

        return resp;
    }

    public BaseResponse getProductList() {
        log.info("get product list.");
        BaseResponse resp = new BaseResponse(ReturnCode.TXN000);
        List<Product> productEntityList = productRepository.findAll();

        List<ProductData> productList = new ArrayList<>();
        for (Product product : productEntityList) {
            ProductData data = new ProductData();
            BeanUtils.copyProperties(product, data);
            productList.add(data);
        }

        resp.setData(productList);
        return resp;
    }

    public BaseResponse getProductById(Long id) {
        log.info("get product by id.");
        BaseResponse resp = new BaseResponse(ReturnCode.TXN000);
        Optional<Product> productOpt = productRepository.findById(id);
        if (productOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "not found product");
        }
        ProductData data = new ProductData();
        BeanUtils.copyProperties(productOpt.get(), data);
        resp.setData(data);
        return resp;
    }

    public BaseResponse updateProduct(Long id, UpdateProductRequest request) {
        BaseResponse resp = new BaseResponse(ReturnCode.TXN000);
        Optional<Product> productOpt = productRepository.findById(id);
        if (productOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "not found product");
        }
        Product newProduct = productOpt.get();
        BeanUtils.copyProperties(request, newProduct);

        productRepository.save(newProduct);

        return resp;
    }

    public BaseResponse deleteProduct(Long id) {
        BaseResponse resp = new BaseResponse(ReturnCode.TXN000);
        Optional<Product> productOpt = productRepository.findById(id);
        if (productOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "not found product");
        }
        productRepository.delete(productOpt.get());
        return resp;
    }




}
